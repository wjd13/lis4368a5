# LIS4368 - Advanced Web Applications Developmment

## Wyatt Dell

### Assignment 5 Deliverables:

1. Provide Bitbucket read-only access to a5 repo, *must* include README.md, using Markdown syntax,which displays assignment requirements, screenshots of pre-, post-valid user form entry

2. Blackboard Links: a5 Bitbucket repo

#### Assignment Screenshots:

![Valid User Form Entry](img/valid.png)

![Passed Validation](img/pass.png)

![Associated Database Entry] (img/dbentry.png)